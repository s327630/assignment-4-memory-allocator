//
// Created by Vadim Klimovich on 30.12.2022.
//
#include "mem.h"
#include "mem_internals.h"

void print_test_title(int testNum, char *title) {
    printf("\n === Test %d: %s ===\n", testNum, title);
}

void test_malloc() {
    print_test_title(1, "malloc");
    void* heap = heap_init(0);
    _malloc(REGION_MIN_SIZE/4);
    _malloc(10);
    debug_heap(stdout, heap);
}

void test_free() {
    print_test_title(2, "free one block");
    void* heap = heap_init(0);
    _malloc(REGION_MIN_SIZE/4);
    void* block_2 = _malloc(REGION_MIN_SIZE/2);
    printf("Before:\n");
    debug_heap(stdout, heap);
    _free(block_2);
    printf("\nAfter:\n");
    debug_heap(stdout, heap);
}

void test_free_2() {
    print_test_title(3, "free two blocks");
    void* heap = heap_init(0);
    _malloc(REGION_MIN_SIZE/5);
    void* block_2 = _malloc(REGION_MIN_SIZE/5);
    _malloc(REGION_MIN_SIZE/5);
    void* block_4 = _malloc(REGION_MIN_SIZE/5);
    printf("Before:\n");
    debug_heap(stdout, heap);
    _free(block_2);
    _free(block_4);
    printf("\nAfter:\n");
    debug_heap(stdout, heap);
}

void test_overflow() {
    print_test_title(4, "overflow");
    void* heap = heap_init(0);
    _malloc(REGION_MIN_SIZE/2);
    printf("Before:\n");
    debug_heap(stdout, heap);
    _malloc(2*REGION_MIN_SIZE/3);
    printf("\nAfter:\n");
    debug_heap(stdout, heap);
}

void test_overflow_2() {
    print_test_title(5, "overflow");
    void* heap = heap_init(0);
    heap_init(0);
    _malloc(REGION_MIN_SIZE/2);
    printf("Before:\n");
    debug_heap(stdout, heap);
    _malloc(5*REGION_MIN_SIZE/2);
    printf("\nAfter:\n");
    debug_heap(stdout, heap);
}

void test_all() {
    test_malloc();
    test_free();
    test_free_2();
    test_overflow();
    test_overflow_2();
}
